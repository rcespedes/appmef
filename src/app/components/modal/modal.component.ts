import { Component, OnInit, OnDestroy, Input, ElementRef, ViewEncapsulation } from '@angular/core';
import { ModalService } from './modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ModalComponent implements OnInit, OnDestroy {

  @Input() id: string;
  private element: any;

  constructor(private modalService: ModalService, private el: ElementRef) {
    this.element = el.nativeElement;
  }

  ngOnInit() {
    // Revisa que el ID del modal exista en su llamado
    if (!this.id) {
      console.error('El modal debe contener un id');
      return;
    }

    // Pone el elemento justo antes de cerrar el body para que esté sobre todo
    document.body.appendChild(this.element);

    // Cierra el modal cuando le dan un click fuera
    // this.element.addEventListener('click', el => {
    //     if (el.target.className === 'app-modal') {
    //       this.close();
    //     }
    // });

    // Se agrega (ésta instancia del modal) al ModalService para ser accesible desde los controladores
    this.modalService.add(this);
  }

  ngOnDestroy() {
    this.modalService.remove(this.id);
    this.element.remove();
  }

  // Abrir modal
  open(): void {
    this.element.style.display = 'block';
    document.body.classList.add('app-modal-open');
  }

  // Cerrar modal
  close(): void {
      this.element.style.display = 'none';
      document.body.classList.remove('app-modal-open');
  }

}
