import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ModalService } from 'src/app/components/modal';
import { AuthService } from 'src/app/services/auth.service';
import { MustMatch } from 'src/app/validators/must-match.validator';

@Component({
  selector: 'app-register-modal',
  templateUrl: './register-modal.component.html',
  styles: [
  ]
})
export class RegisterModalComponent implements OnInit {

  terms = `TÉRMINOS Y CONDICIONES DE USO DEL SERVICIO DE LA VENTANILLA VIRTUAL DEL MINEM

  La Ventanilla Virtual es una herramienta tecnológica que permite realizar trámites, gestionar y hacer seguimiento de sus expedientes virtuales presentados al MINEM.
  
  A través de este medio pueden ingresar sus documentos durante las 24 horas del día, los 365 días del año.
  
  Si requiere modificar algún dato respecto a su usuario o clave de acceso, debe acudir a la Plataforma de Atención al Ciudadano del MINEM. 
  
  I.	REQUISITOS PARA LA PRESTACIÓN DE DOCUMENTOS
  
  Los documentos que se presenten por este medio, deben estar debidamente firmados y digitalizados en formato Adobe Reader PDF, GIF, TIF, JPEG y JPG.
  
  El peso máximo de cada archivo no debe exceder los 10MB, caso contrario, debe dividirse el archivo en tantas partes sean necesarias a fin de no exceder el peso establecido.
  
  En caso de que se presenten títulos valores, los originales de dichos documentos deben ser presentados en la Plataforma de Atención al Ciudadano en un plazo no mayor a dos días hábiles, haciendo referencia al número de expediente generado en la Ventanilla Virtual.
  
  Durante cualquier etapa del proceso del trámite, la entidad de considerarlo necesario, puede solicitar al administrado la presentación de los documentos originales.
  
  II.	HORARIOS Y PLAZOS
  
  La Ventanilla Virtual del MINEM, se encuentra habilitada de lunes a domingo las 24 horas del día.
  
  Para fines del cómputo de plazos, los documentos presentados entre las 00:00 am hasta las 16:50, se consideran presentados el mismo día hábil.
  
  Los documentos presentados después de las 16:50 horas hasta las 23:59 horas, se consideran presentados al día hábil siguiente.
  
  Los documentos presentados los días sábados, domingos y feriados, se consideran presentados al día hábil siguiente.
  
  III.	NOTIFICACIÓN DE LOS DOCUMENTOS PRESENTADOS EN LA VENTANILLA VIRTUAL
  
  Las comunicaciones y notificaciones relacionadas a los documentos presentados por los administrados a través de la Ventanilla Virtual del MINEM, se efectúan de manera virtual al domicilio electrónico del mismo, remitiéndosele para ello una alerta de envío al correo electrónico consignado al momento de su registro.
  
  IV.	RESPONSABILIDADES
  
  El usuario es responsable de la  información que registra y presenta a través de la Ventanilla Virtual. 
  
  En atención al Principio de Presunción de Veracidad establecido en el TUO de la Ley N° 27444, Ley del Procedimiento Administrativo General, se presume que los documentos y declaraciones formuladas por los usuarios responden a la verdad de los hechos que afirman.
  
  La confidencialidad de la clave de acceso a la Ventanilla Virtual, es responsabilidad exclusiva de los usuarios. Si estos deciden delegar en un tercero la responsabilidad de registrar sus documentos, lo hacen bajo su entera responsabilidad. 
  
  V.	RESTRICCIONES DE USO
  
  La Ventanilla Virtual debe usarse exclusivamente para presentar documentos e iniciar trámites ante el MINEM.
  
  La Ventanilla Virtual solo puede ser utilizada con fines lícitos.
  
  VI.	PROTECCIÓN DE DATOS PERSONALES
  
  En cumplimiento de lo dispuesto en la Ley N° 29733, Ley de Protección de Datos Personales, desde el momento de su ingreso y/o utilización del portal, el usuario da expresamente su consentimiento para el tratamiento de los datos personales que por él sean facilitados o que se faciliten a través de su ingreso al portal.
  
  VII.	MARCO LEGAL APLICABLE
  
  -	TUO de la Ley N° 27444, Ley del Procedimiento Administrativo General. 
  -	D.S. N° 004-2013-PCM Modernización de la Gestión Pública.
  -	D.S. Nº 109-2012-PCM Estrategia para la modernización  de la Gestión Pública.
  -	D.S. Nº 066-2011-PCM que aprueba el plan de desarrollo de la sociedad de la información en el Perú La Agenda Digital Peruana  2.0
  -	Resolución Secretarial N° 016-2017-MEM/SEG que aprueba la Directiva N° 002-2017-MEM/SEG denominada “Directiva que regula la Administración Documentaria del Ministerio de Energía y Minas”.`

  registerForm: FormGroup
  fileToUpload: File = null;

  showExtraFields = false;

  constructor(private modalService: ModalService,
              private formBuilder: FormBuilder,
              private authService: AuthService) { }

  ngOnInit(): void {
    this.initForm()
    this.registerForm.controls['userType'].valueChanges.subscribe(value => {
      if (value === 'juridica') {
        this.registerForm.addControl('ruc', new FormControl('', Validators.required))
        this.registerForm.addControl('companyName', new FormControl('', Validators.required))
        this.showExtraFields = true
      } else {
        this.showExtraFields = false
        this.registerForm.removeControl('ruc');
        this.registerForm.removeControl('companyName');
      }
    })
  }

  initForm() {
    this.registerForm = this.formBuilder.group({
      userType: ['natural', Validators.required],
      identityDocumentCode: ['', [Validators.required, Validators.minLength(8)]],
      paternal: [{
        value: '',
        disabled: true
      }, Validators.required],
      maternal: [{
        value: '',
        disabled: true
      }, Validators.required],
      name: [{
        value: '',
        disabled: true
      }, Validators.required],
      mobile: ['', Validators.required],
      phone: ['', Validators.required],
      mail: ['', [Validators.required, Validators.email]],
      repeatMail: ['', [Validators.required, Validators.email]],
      adress: ['', Validators.required],
      department: ['', Validators.required],
      province: ['', Validators.required],
      district: ['', Validators.required],
      birthday: ['', Validators.required],
      verifierCode: ['', Validators.required],
      ubigeoCode: ['', Validators.required],
      agreeTerms: [false, Validators.required],
      underOath: [false, Validators.required],
    }, {
      validator: MustMatch('mail', 'repeatMail')
    })
  }

  searchUserData(document: number) {
    this.paternal.setValue('Perez')
    this.maternal.setValue('Vasquez')
    this.name.setValue('Luisa')
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    // this.authService.uploadIdentityDocumentPDF(this.fileToUpload).subscribe(res => {
    //   console.log(res)
    // })
  }

  submit() {
    const data = this.registerForm.getRawValue()
    this.authService.registerUser(data, this.fileToUpload).subscribe((res: any) => {
      alert(res.status);
      this.closeModal('register-modal');
    })
  }
  
  get userType() {
    return this.registerForm.get('userType');
  }

  get identityDocumentCode() {
    return this.registerForm.get('identityDocumentCode');
  }

  get paternal() {
    return this.registerForm.get('paternal');
  }

  get maternal() {
    return this.registerForm.get('maternal');
  }

  get name() {
    return this.registerForm.get('name');
  }

  get ruc() {
    return this.registerForm.get('ruc');
  }

  get companyName() {
    return this.registerForm.get('companyName');
  }

  get mobile() {
    return this.registerForm.get('mobile');
  }

  get phone() {
    return this.registerForm.get('phone');
  }

  get mail() {
    return this.registerForm.get('mail');
  }

  get repeatMail() {
    return this.registerForm.get('repeatMail');
  }

  get adress() {
    return this.registerForm.get('adress');
  }

  get department() {
    return this.registerForm.get('department');
  }

  get province() {
    return this.registerForm.get('province');
  }

  get district() {
    return this.registerForm.get('district');
  }

  get birthday() {
    return this.registerForm.get('birthday');
  }

  get verifierCode() {
    return this.registerForm.get('verifierCode');
  }

  get ubigeoCode() {
    return this.registerForm.get('ubigeoCode');
  }

  get agreeTerms() {
    return this.registerForm.get('agreeTerms')
  }

  get underOath() {
    return this.registerForm.get('underOath')
  }

  closeModal(id: string) {
    this.modalService.close(id);
  }

}
