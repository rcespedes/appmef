import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoginRoutingModule } from './login-routing.module';
import { ModalModule } from 'src/app/components/modal';

import { LoginComponent } from './login.component';
import { RegisterModalComponent } from '../modals/register-modal/register-modal.component';


@NgModule({
  declarations: [
    LoginComponent,
    RegisterModalComponent
  ],
  imports: [
    CommonModule,
    LoginRoutingModule,
    ModalModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LoginModule { }
