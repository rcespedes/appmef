import { Component, OnInit } from '@angular/core';
import { ModalService } from 'src/app/components/modal';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  public isLoggedIn: Observable<boolean>;

  constructor(private modalService: ModalService,
              private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthService) {
    authService.isLoggedIn().subscribe(status => {
      if (status === true) {
        router.navigateByUrl('/pages/home');
      }
    });
  }

  ngOnInit(): void {
    this.initForm()
  }

  initForm() {
    this.loginForm = this.formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
    })
  }

  submit() {
    const data = this.loginForm.value
    this.authService.loginUser(data).subscribe((res: any) => {
      // if(res.ok === true) {
        const token = 'token'
        this.authService.setToken(token);
        this.router.navigateByUrl('/pages/home')
      // } else if (res.ok === false) {
      //   alert(res.message);
      // }
    })
  }

  get user() {
    return this.loginForm.get('user');
  }

  get password() {
    return this.loginForm.get('password');
  }

  openModal(id: string) {
    this.modalService.open(id);
  }

}
