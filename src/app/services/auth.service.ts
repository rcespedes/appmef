import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { share } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoginSubject = new BehaviorSubject<boolean>(this.hasToken());

  constructor(private http: HttpClient) { }

  registerUser(userData, pdf: File) {
    let endpoint: string;
   /* if (userData.userType === "natural") {
      endpoint = "usuarionatural"
    } else if (userData.userType === "juridica") {
      endpoint = "usuariojuridica"
    }*/

    //userData.nuevocampo = 1;
    const formData = new FormData()
    const headers = new HttpHeaders({
      'mimeType': 'multipart/form-data'
    })

    console.log(userData);
    if (userData.userType === "natural") {
      endpoint = "usuarionatural"
      formData.append('file', pdf, pdf.name);
      formData.append('dni', userData.identityDocumentCode);
      formData.append('apellido_paterno', userData.paternal);
      formData.append('apellido_materno', userData.maternal);
      formData.append('nombres', userData.name);
      formData.append('direccion', userData.adress);
      formData.append('ubigeo', userData.ubigeoCode);
      formData.append('celular', userData.mobile);
      formData.append('correo', userData.mail);
      formData.append('fec_nac', userData.birthday);
    } else if (userData.userType === "juridica") {
      endpoint = "usuariojuridica"
      formData.append('file', pdf, pdf.name);
      formData.append('DNI_REPRESENTANTE', userData.identityDocumentCode);
      formData.append('apellido_paterno_REPRESENTANTE', userData.paternal);
      formData.append('apellido_materno_REPRESENTANTE', userData.maternal);
      formData.append('nombres_REPRESENTANTE', userData.name);
      formData.append('direccion_REPRESENTANTE', userData.adress);
      formData.append('ubigeo_REPRESENTANTE', userData.ubigeoCode);
      formData.append('celular_REPRESENTANTE', userData.mobile);
      formData.append('correo_REPRESENTANTE', userData.mail);
      formData.append('direccion', userData.adress);
      formData.append('ruc', userData.ruc);
      
    }
    
  
    return this.http.post(`${environment.apiUrl}${endpoint}`, formData, {headers})
  }

  loginUser(userData) {
    return this.http.post(`${environment.apiUrl}/login`, userData)
  }

  uploadIdentityDocumentPDF(pdf: File) {
    const formData: FormData = new FormData();
    const headers = new HttpHeaders({
      'Content-type': 'multipart/form-data'
    })
    formData.append('pdf', pdf, pdf.name);
    return this.http.post(`${environment.apiUrl}/login`, formData, { headers })

  }

  logOut(): void {
    this.isLoginSubject.next(false);
    localStorage.removeItem('token');
  }

  isLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable().pipe(share());
  }

  setToken(token: string): void {
    this.isLoginSubject.next(true);
    localStorage.setItem('token', token);
  }

  private hasToken(): boolean {
    return !!localStorage.getItem('token');
  }
}
