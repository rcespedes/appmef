import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  authState: boolean

  constructor(private auth: AuthService, private router: Router) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      this.auth.isLoggedIn().subscribe(status => {
        if (status !== true) {
          this.authState = false;
          this.router.navigateByUrl('/login');
        } else if (status === true) {
          this.authState = true;
        }
      });

      return this.authState;
  }
  
}
